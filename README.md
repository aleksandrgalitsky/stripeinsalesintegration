# Stripe integration

A microservice for Stripe integration for Insales.

## Environment
```
TEST           // e.g. Test
```

## Installation

1. Install the node package

```bash
npm install
```

2. Run the project

```bash
npm start
```

## Log

You can check log in folder storage/logs

### API
1. execute
	- Endpoint : /api/
	- Description : test endpoint
	- Method : POST
	- Params : (Application/json)
		```bash
		- test
		```
	- Response :
		```bash
		- success : true
		```
