const ENV_VARS = [
  "STRIPE_SECRET_KEY",
  "STRIPE_PUBLIC_KEY",
  "PAYMENT_SUCCESS_URL",
  "PAYMENT_FAILED_URL",
  "INSALES_USER",
  "INSALES_PASSWORD",
  "INSALES_HOST",
  "INSALES_PROTOCOL",
  "MONGO_DB",
  "MONGO_USER",
  "MONGO_PASSWORD",
  "MONGO_ADDRESS",
  "NOTIFICATION_EMAIL_FROM",
  "NOTIFICATION_EMAILS_TO",
  "NOTIFICATION_HOST",
  "NOTIFICATION_PORT",
  "NOTIFICATION_SECURE",
  "NOTIFICATION_USER",
  "NOTIFICATION_PASSWORD",
  "NODE_PORT"
];

export const STRIPE_SECRET_KEY = process.env.STRIPE_SECRET_KEY;                     // e.g. sk_test_.....
export const STRIPE_PUBLIC_KEY = process.env.STRIPE_PUBLIC_KEY;                     // e.g. pk_test_.....
export const PAYMENT_SUCCESS_URL = process.env.PAYMENT_SUCCESS_URL;                 // e.g. /payments/external/1039968/success
export const PAYMENT_FAILED_URL = process.env.PAYMENT_FAILED_URL;                   // e.g. /payments/external/1039968/fail
export const INSALES_USER = process.env.INSALES_USER;                               // e.g. fb19c3f0.....
export const INSALES_PASSWORD = process.env.INSALES_PASSWORD;                       // e.g. 5ca845f0c....
export const INSALES_HOST = process.env.INSALES_HOST;                               // e.g. myshop-xxxxxx.myinsales.ru
export const INSALES_PROTOCOL = process.env.INSALES_PROTOCOL;                       // e.g. http:// or https://
export const MONGO_DB = process.env.MONGO_DB;                                       // e.g. insales 
export const MONGO_USER = process.env.MONGO_USER;                                   // e.g. insales
export const MONGO_PASSWORD = process.env.MONGO_PASSWORD;                           // e.g. passw0rd
export const MONGO_ADDRESS = process.env.MONGO_ADDRESS;                             // e.g. localhost:27017
export const NOTIFICATION_EMAIL_FROM = process.env.NOTIFICATION_EMAIL_FROM;         // e.g. from@mail.com
export const NOTIFICATION_EMAILS_TO = process.env.NOTIFICATION_EMAILS_TO;           // e.g. to@mail.com or to@mail.com, to2@mail.com
export const NOTIFICATION_HOST = process.env.NOTIFICATION_HOST;                     // e.g. smtp.ethereal.email
export const NOTIFICATION_PORT = process.env.NOTIFICATION_PORT;                     // e.g. 587
export const NOTIFICATION_SECURE = process.env.NOTIFICATION_SECURE;                 // e.g. false or true
export const NOTIFICATION_USER = process.env.NOTIFICATION_USER;                     // e.g. from@mail.com
export const NOTIFICATION_PASSWORD = process.env.NOTIFICATION_PASSWORD;             // e.g. passw0rd
export const NODE_PORT = process.env.NODE_PORT;                                     // e.g. 80

export const ValidEnv = function () {
  let invalids = [];
  for (let i in ENV_VARS) {
    let key = ENV_VARS[i];
    if (process.env[key] === undefined) {
      invalids.push(key);
    }
    console.log(process.env[key]);
  }
  if (invalids.length > 0) {
    console.log("INVALID ENVIRONMENT VARIABLES:", invalids);
    return false;
  }
  return true;
}