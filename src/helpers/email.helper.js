const nodemailer = require('nodemailer');
import {
  NOTIFICATION_EMAIL_FROM,
  NOTIFICATION_EMAILS_TO,
  NOTIFICATION_HOST,
  NOTIFICATION_PORT,
  NOTIFICATION_SECURE,
  NOTIFICATION_USER,
  NOTIFICATION_PASSWORD
} from "../helpers/env.helper";

const sender = "Stripe microservice";

const getTransport = async function () {
  let transporter = nodemailer.createTransport({
    host: NOTIFICATION_HOST,
    port: NOTIFICATION_PORT,
    secure: NOTIFICATION_SECURE,
    auth: {
      user: NOTIFICATION_USER,
      pass: NOTIFICATION_PASSWORD
    }
  });
  return transporter;
}

export default {
  chargeFailed: async (text, html) => {
    let transporter = await getTransport();
    return await transporter.sendMail({
      from: `${sender} <${NOTIFICATION_EMAIL_FROM}>`,
      to: NOTIFICATION_EMAILS_TO,
      subject: "Stripe MS: Charge FAILED",
      text: text,
      html: html
    });
  },
  chargeSuccess: async (text, html) => {
    let transporter = await getTransport();
    return await transporter.sendMail({
      from: `${sender} <${NOTIFICATION_EMAIL_FROM}>`,
      to: NOTIFICATION_EMAILS_TO,
      subject: "Stripe MS: Charge SUCCESS",
      text: text,
      html: html
    });
  },
  exception: async (text, html) => {
    let transporter = await getTransport();
    return await transporter.sendMail({
      from: `${sender} <${NOTIFICATION_EMAIL_FROM}>`,
      to: NOTIFICATION_EMAILS_TO,
      subject: "Stripe MS: Exception",
      text: text,
      html: html
    });
  },
  send: async (to, subject, text, html) => {
    let transporter = await getTransport();
    return await transporter.sendMail({
      from: `${sender} <${NOTIFICATION_EMAIL_FROM}>`,
      to: to,
      subject: subject,
      text: text,
      html: html
    });
  }
}