const MongoClient = require('mongodb').MongoClient;
import {
  MONGO_ADDRESS,
  MONGO_DB,
  MONGO_USER,
  MONGO_PASSWORD
} from "../helpers/env.helper";

const mongoClient = new MongoClient(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_ADDRESS}/databaseName?authSource=${MONGO_DB}`);

export const InsertOrder = function (order) {
  return new Promise((resolve, reject) => {
    mongoClient.connect((err, client) => {
      if (err) {
        reject(err);
      } else {
        const Db = client.db(MONGO_DB);
        let ordersCollection = Db.collection('InsalesOrder');
        ordersCollection.insertOne(order, function (err, result) {
          if (err) {
            reject(err);
            console.log('Order insert error: ', err);
          }
          console.log('Order inserted: order:', order._id);

          resolve(order._id);
        });
      }
    });
  });
};

export const InsertSession = function (session) {
  return new Promise((resolve, reject) => {
    mongoClient.connect((err, client) => {
      if (err) {
        reject(err);
      } else {
        const Db = client.db(MONGO_DB);
        let sessionsCollection = Db.collection('StripeSession');
        sessionsCollection.insertOne(session, function (err, result) {
          if (err) {
            reject(err);
            console.log('Session insert error: ', err);
          }
          console.log('Session inserted: id=', session._id);

          resolve(session._id);
        });
      }
    });
  });
};

export const UpdateSession = function (session) {
  return new Promise((resolve, reject) => {
    mongoClient.connect((err, client) => {
      if (err) {
        reject(err);
      } else {
        const Db = client.db(MONGO_DB);
        let sessionsCollection = Db.collection('StripeSession');
        sessionsCollection.update(
          { id: session.id },
          session, { upsert: true },
          function (err, result) {
            if (err) {
              reject(err);
            }
            const res = sessionsCollection.find({ id: session.id }).toArray(function (err, items) {
              resolve(items[0]._id);
            });
          }
        );
      }
    });
  });
};

export const InsertOrderSession = function (orderSession) {
  return new Promise((resolve, reject) => {
    mongoClient.connect((err, client) => {
      if (err) {
        reject(err);
      } else {
        const Db = client.db(MONGO_DB);
        let orderSessionsCollection = Db.collection('OrderSession');
        orderSessionsCollection.insertOne(orderSession, function (err, result) {
          if (err) {
            reject(err);
            console.log('OrderSession insert error: ', err);
          }
          console.log('OrderSession inserted: id=', orderSession._id);

          resolve(orderSession._id);
        });
      }
    });
  });
};

export const GetOrderBySessionId = function (sessionId) {
  return new Promise((resolve, reject) => {
    mongoClient.connect((err, client) => {
      if (err) {
        reject(err);
      } else {
        const Db = client.db(MONGO_DB);
        let orderSessionsCollection = Db.collection('OrderSession');
        orderSessionsCollection.find({ sessionId: sessionId }).toArray(function (err, os) {
          let orderCollection = Db.collection('InsalesOrder');
          orderCollection.find({ _id: os[0].orderId }).toArray(function (err, items) {
            resolve(JSON.parse(items[0].order_json));
          });
        });        
      }
    });
  });
};

export const InsertCharge = function (charge) {
  return new Promise((resolve, reject) => {
    mongoClient.connect((err, client) => {
      if (err) {
        reject(err);
      } else {
        const Db = client.db(MONGO_DB);
        let chargeCollection = Db.collection('StripeCharge');
        chargeCollection.insertOne(charge, function (err, result) {
          if (err) {
            reject(err);
            console.log('Charge insert error: ', err);
          }

          resolve({ id: charge._id });
        });
      }
    });
  });
};