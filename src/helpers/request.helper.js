import request from 'request';
//import log from '../loggers';
export const CallRequest = async (options) => {
  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      if (error) {
        return reject(error);
      }
      return resolve(body);
    });
  })
}

const serialize = function (obj) {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}


export const CallGet = async (url, data) => {
  let options = {
    method: 'GET',
    url: url + "?" + serialize(data),
    headers: { 'cache-control': 'no-cache' }
  };
  console.log("Call GET :", options);
  //log.info("=======Call GET Request=======");
  //log.info(options);
  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      if (error) {
        return resolve(false);
      }
      console.log(body)
      //log.info(response);
      return resolve(response.statusCode);
    });
  })
}

