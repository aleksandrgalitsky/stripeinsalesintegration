import { ErrorHanlder, SuccessHandler } from '../middleware/response.handler';
import userService from '../services/main.service';
import {
  STRIPE_PUBLIC_KEY
} from "../helpers/env.helper";

export const MainController = {
  checkout: async (req, res, next) => {
    try {
      var result = await userService.checkout(req.body);
      
      res.render("checkout.hbs", {
        session: result,
        publicKey: STRIPE_PUBLIC_KEY
      });
    }
    catch (e) {
      console.log("error:", e);
      return ErrorHanlder(res, {
        status: 400,
        message: e,
        code: 400
      });
    }
  },
  webhook: async (req, res, next) => {
    try {
      let event = req.body;

      switch (event.type) {
        case 'charge.succeeded':
          await userService.chargeSuccess(event.data.object);
          break;
        case 'charge.failed':
          await userService.chargeFailed(event.data.object);
          break;
        case 'checkout.session.completed':
          await userService.sessionCompleted(event.data.object);
          break;
        default:
          return ErrorHanlder(res, {
            status: 400,
            message: 'unhandled event',
            code: 400
          });
      }

      return SuccessHandler(res, { received: true });
    }
    catch (e) {
      console.log("error:", e);
      return ErrorHanlder(res, {
        status: 400,
        message: e,
        code: 400
      });
    }
  }
}