
import express from "express";
import userRoute from './main.route';

const Router = express.Router();
Router.use('/stripe', userRoute);
export default Router;
