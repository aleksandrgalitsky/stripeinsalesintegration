import express from "express";
import { MainController } from '../controllers/main.controller'

const Router = express.Router();

Router.post("/webhook", MainController.webhook);
Router.post("/checkout", MainController.checkout);

export default Router;
