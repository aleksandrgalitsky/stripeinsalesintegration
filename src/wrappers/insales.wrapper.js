import { CallRequest } from '../helpers/request.helper';
import {
  INSALES_USER,
  INSALES_PASSWORD,
  INSALES_HOST,
  INSALES_PROTOCOL
} from "../helpers/env.helper";
import { InsalesAPI } from '../constants/insales-api.constants';

const GetURL = function (endpoint, params) {
  return `${INSALES_PROTOCOL}${INSALES_USER}:${INSALES_PASSWORD}@${INSALES_HOST}${endpoint}${params}.json`;
}

export class InsalesApiWrapper {
  static async Products(id) {
    let options = {
      method: "GET",
      uri: GetURL(InsalesAPI.Products, `/${id}`),
      headers: {
        "Content-Type": "application/json"
      },
      json: true
    }
    var result = await CallRequest(options);
    return result;
  }
  static async OrderPaid(id) {
    let options = {
      method: "PUT",
      uri: GetURL(InsalesAPI.Orders, `/${id}`),
      headers: {
        "Content-Type": "application/json"
      },
      body: {
        "order": {
          "financial_status": "paid"
        }
      },
      json: true
    }
    var result = await CallRequest(options);
    return result;
  }
}