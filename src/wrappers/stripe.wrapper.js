import { v4 as uuidv4 } from 'uuid';
import {
  STRIPE_SECRET_KEY,
  INSALES_PROTOCOL,
  INSALES_HOST,
  PAYMENT_FAILED_URL,
  PAYMENT_SUCCESS_URL
} from "../helpers/env.helper";

export class StripeWrapper {
  static async createSession(lineItems) {
    const stripe = require('stripe')(STRIPE_SECRET_KEY);
    const randId = uuidv4();
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: lineItems,
      success_url: `${INSALES_PROTOCOL}${INSALES_HOST}${PAYMENT_SUCCESS_URL}?session_id=${randId}`,
      cancel_url: `${INSALES_PROTOCOL}${INSALES_HOST}${PAYMENT_FAILED_URL}`
    });
    return session;
  }
}