export const ErrorHanlder = (res, obj) => {
  res.status(obj.status).json({
    success: false,
    message: obj.message,
    code: obj.code,
    data: obj.data
  });
};

export const SuccessHandler = (res, data) => {
  res.json({
    success: true,
    data: data
  });
};

export const RawResponseHandler = (res, data) => {
  res.json(data);
}
