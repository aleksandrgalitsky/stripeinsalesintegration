import { InsalesApiWrapper } from '../wrappers/insales.wrapper';
import {
  InsertOrder,
  InsertSession,
  UpdateSession,
  InsertCharge,
  InsertOrderSession,
  GetOrderBySessionId
} from '../helpers/db.helper';
import emailHelper from '../helpers/email.helper';
import { StripeWrapper } from '../wrappers/stripe.wrapper';
import {
  NOTIFICATION_EMAIL_FROM
} from "../helpers/env.helper";

export default {
  checkout: async (order) => {
    try {
      const orderId = await InsertOrder(order);

      let lineItems = [];
      let order_json = JSON.parse(order.order_json);

      for (const element of order_json.order_lines) {
        let product = await InsalesApiWrapper.Products(element.product_id);
        let images = product.images.map(item => {
          return item.small_url;
        });

        lineItems.push({
          name: product.title,
          description: product.short_description,
          images: images,
          amount: element.full_sale_price * 100,
          currency: order_json.currency_code.toLowerCase(),
          quantity: element.quantity,
        });
      }

      if (order_json.full_delivery_price > 0) {
        lineItems.push({
          name: `Delivery: ${order_json.delivery_title}`,
          description: order_json.short_description,
          images: [],
          amount: order_json.full_delivery_price * 100,
          currency: order_json.currency_code.toLowerCase(),
          quantity: 1,
        });
      }

      const session = await StripeWrapper.createSession(lineItems);

      const sessionId = await InsertSession(session);

      InsertOrderSession({ orderId: orderId, sessionId: sessionId });

      return session.id;
    } catch (err) {
      return err;
    }
  },
  chargeSuccess: async (charge) => {
    try {
      InsertCharge(charge);
      if (NOTIFICATION_EMAIL_FROM !== '') {
        emailHelper.chargeSuccess("success", "<strong>SUCCESS</strong>");
      }
    } catch (err) {
      return err;
    }
  },
  chargeFailed: async (charge) => {
    try {
      InsertCharge(charge);
      if (NOTIFICATION_EMAIL_FROM !== '') {
        emailHelper.chargeFailed("failed", "<strong>FAILED</strong>");
      }
    } catch (err) {
      return err;
    }
  },
  sessionCompleted: async (session) => {
    try {
      await UpdateSession(session).then(async (res) => {
        console.log('UpdateSession', res);
        await GetOrderBySessionId(res).then(async (res2) => {
          console.log('GetOrderBySessionId', res2);
          InsalesApiWrapper.OrderPaid(res2.id);
        }).catch(err => {
            console.log("ERROR** :", err);
          });
      }).catch(err => {
        console.log("ERROR** :", err);
      });
    } catch (err) {
      return err;
    }
  }
}
