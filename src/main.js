import bodyParser from "body-parser";
import Express from "express";
import cors from "cors";
import morgan from "morgan";
import route from "./routes";
import { ValidEnv } from './helpers/env.helper';
const https = require('https');
const fs = require('fs');

const options = {
  cert: fs.readFileSync('./fullchain.pem'),
  key: fs.readFileSync('./privkey.pem')
};

const app = Express();

app.set("view engine", "hbs");
app.set("views", "./src/views/"); // установка пути к представлениям
app.use(Express.static('public'));

//app.use(bodyParser.raw({ type: 'application/jwt' }));
app.use(cors());
app.use(
  bodyParser.json({
    limit: "100mb"
  })
);

app.use(
  bodyParser.urlencoded({
    limit: "100mb",
    extended: true
  })
);

app.use(morgan("dev"));

app.use("/api", route);
app.use((err, req, res, next) => {
  if (err) {
    if (err.status != undefined) {
      return res.status(err.status).send();
    }
    else {
      res.status(400);
      return res.send({
        success: false,
        message: "An error occurred. Please try again"
      });
    }
  } else {
    next();
  }
});
const port = process.env.NODE_PORT || 8808;
console.log(port);

//verify env data
if (ValidEnv()) {
  app.listen(port, () => console.log(`StripeIntegration start on port  ${port} `));
  https.createServer(options, app).listen(443);
}